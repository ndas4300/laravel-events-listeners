<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Post Form') }}
        </h2>
    </x-slot>

    <body>

        <div class="container mt-3">
            {{ Form::open(['route' => 'post.store']) }}
            <div class="card-body">
                <div class="form-group">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control',
                        'required' => 'required']) }}
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                        {{ Form::label('description', 'Description') }}
                        {{ Form::textarea('description', null, ['placeholder' => 'Write Description', 'class'
                        => 'form-control']) }}
                        <small class="text-danger">{{ $errors->first('description') }}</small>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">{{ __('Submit')}}</button>
            </div>

            {{ Form::close() }}
        </div>

    </body>
</x-app-layout>
